<?php

declare(strict_types=1);

namespace App\Factories;

use App\Interfaces\UserFactoryInterface;
use App\Models\User;

class RandomUserFactory implements UserFactoryInterface
{
    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        return User::make([
            'title' => $data['name']['title'],
            'first_name' => $data['name']['first'],
            'last_name' => $data['name']['last'],
            'country' => $data['location']['country'],
            'phone' => $data['phone'],
            'email' => $data['email'],
        ]);
    }
}
