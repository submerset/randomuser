<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UsersIndexRequest;
use App\Interfaces\FetchUserServiceInterface;
use App\Query\FetchUserQuery;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function __construct(private FetchUserServiceInterface $userService)
    {
    }

    public function index(UsersIndexRequest $request): Response
    {
        $query = new FetchUserQuery();
        $query->limit = $request->integer('limit');
        $query->sort = $request->get('sort');
        $data = $this->userService->fetch($query);

        return response()->xml('user', $data->toArray());
    }
}
