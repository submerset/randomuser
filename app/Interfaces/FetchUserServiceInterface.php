<?php
declare(strict_types=1);

namespace App\Interfaces;

use App\Models\User;
use App\Query\FetchUserQuery;
use Illuminate\Support\Collection;

interface FetchUserServiceInterface
{
    /**
     * @param FetchUserQuery $query
     * @return Collection<User>
     */
    public function fetch(FetchUserQuery $query): Collection;
}
