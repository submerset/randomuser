<?php

namespace App\Interfaces;

use App\Models\User;

interface UserFactoryInterface
{
    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User;
}
