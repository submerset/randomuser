<?php

namespace App\Providers;

use App\Factories\RandomUserFactory;
use App\Interfaces\FetchUserServiceInterface;
use App\Interfaces\UserFactoryInterface;
use App\Services\RandomUserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->register(ResponseXmlServiceProvider::class);
        $this->app->bind(FetchUserServiceInterface::class, RandomUserService::class);
        $this->app->bind(UserFactoryInterface::class, RandomUserFactory::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
