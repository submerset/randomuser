<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Http\Response;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use Spatie\ArrayToXml\ArrayToXml;

class ResponseXmlServiceProvider extends ServiceProvider
{

    public function register(): void
    {
    }

    public function boot(): void
    {
        ResponseFactory::macro('xml', function (string $name, array $data, $status = 200) {
            return new Response(
                ArrayToXml::convert([
                    'data' => [
                        $name => $data,
                    ]
                ]), $status, ['Content-Type' => 'application/xml']
            );
        });
    }
}
