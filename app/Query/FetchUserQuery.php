<?php

namespace App\Query;

class FetchUserQuery
{
    public ?int $limit = null;
    public ?string $sort = null;
}
