<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\FetchUserException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class RandomUserApi
{
    private Client $http;

    function __construct()
    {
        $uri = config('randomuser')['uri'];
        $this->http = new Client([
            'base_uri' => $uri
        ]);
    }

    /**
     * @param int $limit
     * @return array
     * @throws FetchUserException
     * @throws GuzzleException
     */
    public function fetch(int $limit): array
    {
        $response = $this->http->get('/api', ['query' => ['results' => $limit]]);

        if ($response->getStatusCode() !== ResponseAlias::HTTP_OK) {
            throw new FetchUserException($response->getBody()->getContents(), $response->getStatusCode());
        }

        return json_decode(
            $response->getBody()
                ->getContents()
            ,
            true
        )['results'];
    }
}