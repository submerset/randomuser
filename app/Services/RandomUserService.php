<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\FetchUserException;
use App\Interfaces\FetchUserServiceInterface;
use App\Interfaces\UserFactoryInterface;
use App\Query\FetchUserQuery;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class RandomUserService implements FetchUserServiceInterface
{

    public function __construct(private RandomUserApi $api, private UserFactoryInterface $userFactory)
    {
    }

    /**
     * @throws FetchUserException
     * @throws GuzzleException
     */
    public function fetch(FetchUserQuery $query): Collection
    {
        return collect($this->api->fetch($query->limit))
            ->map(fn(array $data) => $this->userFactory->create($data))
            ->sortByDesc($query->sort)
            ->values();
    }
}
